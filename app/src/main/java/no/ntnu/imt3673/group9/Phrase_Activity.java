package no.ntnu.imt3673.group9;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Phrase_Activity extends AppCompatActivity {

    //Stores received intent
    Intent intent;
    //Encoded image String
    String imgEncoded;
    //ParticipantId
    String pId;
    //RoomId
    String rId;
    //GameId
    String gId;
    //Next player's id
    String nId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrase_);

        intent = getIntent();

        pId = intent.getStringExtra("participantId");
        rId = intent.getStringExtra("roomId");
        gId = intent.getStringExtra("gameId");
        nId = intent.getStringExtra("fetchId");

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        Log.d("ROOM","FETCHING: " + rId + " " + gId + " " + nId);

        databaseReference.child(rId).child(gId).child(nId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    imgEncoded = String.valueOf(dataSnapshot.getValue());

                    //Decodes the image and sets it in the imageView
                    ImageView imgView = findViewById(R.id.imageView);
                    byte[] decodedString = Base64.decode(imgEncoded, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imgView.setImageBitmap(decodedByte);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("FIREBASE", "Read of firebase failed");
            }
        });

            Button sendBtn = findViewById(R.id.sendPhrase);

            final TextView textView = findViewById(R.id.phrase);

            sendBtn.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View v){

                String phrase = textView.getText().toString();

                Log.d("TEST", "pId: " + pId + " rId: " + rId + " gId: " + gId);

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

                databaseReference.child(rId).child(gId).child(pId).setValue(phrase);

                setResult(RESULT_OK);
                finish();
            }
            }
        );
        }
    }
