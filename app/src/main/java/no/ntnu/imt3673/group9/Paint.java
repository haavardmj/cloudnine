package no.ntnu.imt3673.group9;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;

public class Paint extends AppCompatActivity {

    private PaintView paintView;
    private Intent intent;
    private Room mRoom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);

        paintView = (PaintView) findViewById(R.id.paintView);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintView.init(metrics);

        intent = getIntent();
        TextView phraseView = findViewById(R.id.phrase);
        Log.d("GUESS", intent.getStringExtra("phrase"));
        phraseView.setText("DRAW: " + intent.getStringExtra("phrase"));
       // phraseView.append(intent.getStringExtra("phrase"));

        mRoom = intent.getParcelableExtra("room");

        Button send = findViewById(R.id.sendPhrase);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ByteArrayOutputStream bArrayOS = new ByteArrayOutputStream();
                paintView.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, bArrayOS);
                byte[] bArray = bArrayOS.toByteArray();
                String encoded = Base64.encodeToString(bArray, Base64.DEFAULT);
                String pId = intent.getStringExtra("participantId");
                String rId = intent.getStringExtra("roomId");
                String gId = intent.getStringExtra("gameId");

                Log.d("TEST", "pId: " + pId + " rId: " + rId + " gId: " + gId);

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

                databaseReference.child(rId).child(gId).child(pId).setValue(encoded);

                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.paintmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.normal:
                paintView.normal();
                return true;
            case R.id.emboss:
                paintView.emboss();
                return true;
            case R.id.blur:
                paintView.blur();
                return true;
            case R.id.clear:
                paintView.clear();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

   /* void sendToAllReliably(byte[] message) {
        for (final String participantId : mRoom.getParticipantIds()) {
            if (!participantId.equals(mMyParticipantId)) {
                Task<Integer> task = Games.
                        getRealTimeMultiplayerClient(this, GoogleSignIn.getLastSignedInAccount(this))
                        .sendReliableMessage(message, mRoom.getRoomId(), participantId,
                                handleMessageSentCallback).addOnCompleteListener(new OnCompleteListener<Integer>() {
                            @Override
                            public void onComplete(@NonNull Task<Integer> task) {
                                // Keep track of which messages are sent, if desired.
                                //recordMessageToken(task.getResult());
                            }
                        });
            }
        }
    }*/
}