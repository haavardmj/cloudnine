package no.ntnu.imt3673.group9;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesCallbackStatusCodes;
import com.google.android.gms.games.RealTimeMultiplayerClient;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.OnRealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateCallback;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
* Connection logic built heavily on SAMPLE file: https://github.com/playgameservices/android-basic-samples/blob/master/ButtonClicker/src/main/java/com/google/example/games/bc/MainActivity.java
*/
public class GameTest extends AppCompatActivity {

    //Google Play Games activities ids
    private static final int RC_SELECT_PLAYERS = 9006;
    private static final int RC_WAITING_ROOM = 9007;
    private static final int RC_INVITATION_INBOX = 9008;

    //Return code for drawing activity
    private static final int DRAWING_CODE = 1234;
    //Return code for phrase activity
    private static final int PHRASE_CODE = 5678;

    //Multilayer room config
    RoomConfig mJoinedRoomConfig;

    //The game instance id
    private int gameId;

    private boolean firstTurn = true;

    //player index. Starts at 1!
    String playerIndexString = "";

    // are we already playing?
    boolean mPlaying = false;

    // at least 2 players required for a game and max 8
    final static int MIN_PLAYERS = 2;
    final static int MAX_PLAYERS = 8;

    //The room the game takes place in
    private Room mRoom;

    //your participantId
    String mMyParticipantId = null;

    //The player you will recieve data from
    String nextPlayerId = null;

    // Invitation to be sendt
    Invitation invitation = null;

    //FireBase reference
    private DatabaseReference databaseReference;

    //Player array
    private String[] playerArray;

    //Message ID
    private String mID = "";

    //Boolean to hinder triggering more than once from broadcast
    private boolean turn = false;

    // Message container for broadcasting
    private String msg = "";

    //Used to determine if a player is ready or not. 0 = not ready. 1 = ready.
    private boolean[] readyList;

    //Keeps track of which turn it is
    private int turnCounter = 1;

    //The phrase used in the game
    private String phrase = "";

    //The encoded image
    private String image = "";

    /**
     * Handles room updates
     */
    private RoomUpdateCallback mRoomUpdateCallback = new RoomUpdateCallback() {
        @Override
        public void onRoomCreated(int i, @Nullable Room room) {
            // Update UI and internal state based on room updates.
            if (i == GamesCallbackStatusCodes.OK && room != null) {
                Log.d("ROOM", "Room " + room.getRoomId() + " created.");
                Log.d("ROOM", "Room created. Waiting for more players");
                showWaitingRoom(room, MAX_PLAYERS);
            } else {
                Log.w("ROOM", "Error creating room: " + i);
                // let screen go to sleep
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            }
        }

        @Override
        public void onJoinedRoom(int i, @Nullable Room room) {
            //Log.d("ROOM", "A player joined the room");
            // Update UI and internal state based on room updates.
            if (i == GamesCallbackStatusCodes.OK && room != null) {
                Log.d("ROOM", "Room " + room.getRoomId() + " joined.");
                showWaitingRoom(room, MAX_PLAYERS);
            } else {
                Log.w("ROOM", "Error joining room: " + i);
                // let screen go to sleep
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            }
        }

        @Override
        public void onLeftRoom(int i, @NonNull String s) {
            Log.d("ROOM", "A player left the room: " + s);
        }

        @Override
        public void onRoomConnected(int i, @Nullable Room room) {
            //Log.d("ROOM", "The game is ready to start!");

            if (i == GamesCallbackStatusCodes.OK && room != null) {
                Log.d("ROOM", "Room " + room.getRoomId() + " connected.");
                //showWaitingRoom(room,MAX_PLAYERS);
            } else {
                Log.w("ROOM", "Error connecting to room: " + i);
                // let screen go to sleep
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            }
        }
    };

    /**
     * Handles incoming messages
     */
    private OnRealTimeMessageReceivedListener mMessageReceivedHandler = new OnRealTimeMessageReceivedListener() {
        @Override
        public void onRealTimeMessageReceived(@NonNull RealTimeMessage realTimeMessage) {
            String messageRecieved = new String(realTimeMessage.getMessageData());
            Log.d("MESSAGE", "Received this message: " + messageRecieved);

            String[] message = messageRecieved.split("\\|");


            switch (message[0]) {
                case "pIds":
                    if (firstTurn){
                    Log.d("ROOM", "Case pIds");
                    activitySwitch(message[1]);
                    }
                    break;
                case "sent":
                    Log.d("ROOM", "Case sent");
                    break;
                case "end":
                    Log.d("ROOM", "Case end");
                    break;
                case "ready":
                    setParticipantReady(message[1], true);
                    if(arePlayersReady() && !turn){
                        turn = true;
                        activitySwitch(null);
                    }
                    break;
                default:
                    Log.e("MESSAGE", "Wrong format on message received. Message received: " + messageRecieved);
            }

        }
    };

    /**
     * Handles outgoing messages
     */
    private RealTimeMultiplayerClient.ReliableMessageSentCallback handleMessageSentCallback = new RealTimeMultiplayerClient.ReliableMessageSentCallback() {
        @Override
        public void onRealTimeMessageSent(int i, int i1, String s) {
            Log.d("MESSAGE", "sent message: " + s);
        }
    };

    // returns whether there are enough players to start the game
    boolean shouldStartGame(Room room) {
        int connectedPlayers = 0;
        for (Participant p : room.getParticipants()) {
            if (p.isConnectedToRoom()) {
                ++connectedPlayers;
            }
        }
        return connectedPlayers >= MIN_PLAYERS;
    }


    // Returns whether the room is in a state where the game should be canceled.
    boolean shouldCancelGame(Room room) {
        // TODO: Cleanup of room, notification for players
        return false;
    }


    /**
     * Handles room callback updates
     */
    private Activity thisActivity = this;
    //private Room mRoom;

    private RoomStatusUpdateCallback mRoomStatusCallbackHandler = new RoomStatusUpdateCallback() {
        @Override
        public void onRoomConnecting(@Nullable Room room) {
            Log.d("ROOMSTATUS", "Room Connecting");
        }

        @Override
        public void onRoomAutoMatching(@Nullable Room room) {
            Log.d("ROOMSTATUS", "Auto Matching");
        }

        @Override
        public void onPeerInvitedToRoom(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "Peer Invited");
        }

        @Override
        public void onPeerDeclined(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "Peer Declined");

            // Peer declined invitation, see if game should be canceled

            if (!mPlaying && shouldCancelGame(room)) {
                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(thisActivity))
                        .leave(mJoinedRoomConfig, room.getRoomId());
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }

        @Override
        public void onPeerJoined(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "Peer Joined");
            for (String player : list) {
                Log.d("ROOMSTATUS", "Player: " + player + " joined the room");

            }
        }

        @Override
        public void onPeerLeft(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "Peer Left");
            // Peer left, see if game should be canceled.
            if (!mPlaying && shouldCancelGame(room)) {
                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(thisActivity))
                        .leave(mJoinedRoomConfig, room.getRoomId());
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }

        @Override
        public void onConnectedToRoom(@Nullable Room room) {

            Log.d("ROOMSTATUS", "Connected to room");

            // Connected to room, record the room Id.
            mRoom = room;
            Games.getPlayersClient(thisActivity, GoogleSignIn.getLastSignedInAccount(thisActivity))
                    .getCurrentPlayerId().addOnSuccessListener(new OnSuccessListener<String>() {
                @Override
                public void onSuccess(String playerId) {
                    mMyParticipantId = mRoom.getParticipantId(playerId);
                }
            });
        }

        @Override
        public void onDisconnectedFromRoom(@Nullable Room room) {
            Log.d("ROOMSTATUS", "Disconnected from room");
            // This usually happens due to a network error, leave the game.
            Games.getRealTimeMultiplayerClient(thisActivity, GoogleSignIn.getLastSignedInAccount(thisActivity))
                    .leave(mJoinedRoomConfig, room.getRoomId());
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            // show error message and return to main screen
            mRoom = null;
            mJoinedRoomConfig = null;
        }

        @Override
        public void onPeersConnected(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "peers connected");
            if (mPlaying) {
                // add new player to an ongoing game
            } else if (shouldStartGame(room)) {
                // start game!
            }
        }

        @Override
        public void onPeersDisconnected(@Nullable Room room, @NonNull List<String> list) {
            Log.d("ROOMSTATUS", "peers disconnected");
            if (mPlaying) {
                // do game-specific handling of this -- remove player's avatar
                // from the screen, etc. If not enough players are left for
                // the game to go on, end the game and leave the room.
            } else if (shouldCancelGame(room)) {
                // cancel the game
                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(thisActivity))
                        .leave(mJoinedRoomConfig, room.getRoomId());
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }

        @Override
        public void onP2PConnected(@NonNull String s) {
            Log.d("ROOMSTATUS", "P2P connected");
        }

        @Override
        public void onP2PDisconnected(@NonNull String s) {
            Log.d("ROOMSTATUS", "");
        }
    };

    private void showWaitingRoom(Room room, int maxPlayersToStartGame) {
        Games.getRealTimeMultiplayerClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .getWaitingRoomIntent(room, maxPlayersToStartGame)
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_WAITING_ROOM);
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_test);

        //thisActivity = this;

        Button startGame = findViewById(R.id.startGame);
        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitePlayers();
            }
        });

        Button inv = findViewById(R.id.inv);
        inv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInvitationInbox();
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * Starts an activity for inviting players
     */
    private void invitePlayers() {
        // launch the player selection screen
        Games.getRealTimeMultiplayerClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .getSelectOpponentsIntent(1, 6, true)   //Min players = 1. Max = 8.
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_SELECT_PLAYERS);
                    }
                });
    }

    /**
     * starts an activity for checking invitations
     */
    private void showInvitationInbox() {
        Games.getInvitationsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .getInvitationInboxIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_INVITATION_INBOX);
                    }
                });
    }

    // returns whether there are enough players to start the game


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //SELECT PLAYERS TO INVITE
        if (requestCode == RC_SELECT_PLAYERS) {
            if (resultCode != Activity.RESULT_OK) {
                // Canceled or some other error.
                return;
            }

            // Get the invitee list.
            final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // Get Automatch criteria.
            int minAutoPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

            // Create the room configuration.
            RoomConfig.Builder roomBuilder = RoomConfig.builder(mRoomUpdateCallback)
                    .setOnMessageReceivedListener(mMessageReceivedHandler)
                    .setRoomStatusUpdateCallback(mRoomStatusCallbackHandler)
                    .addPlayersToInvite(invitees);
            if (minAutoPlayers > 0) {
                roomBuilder.setAutoMatchCriteria(
                        RoomConfig.createAutoMatchCriteria(minAutoPlayers, maxAutoPlayers, 0));
            }

            // Save the roomConfig so we can use it if we call leave().
            mJoinedRoomConfig = roomBuilder.build();
            Games.getRealTimeMultiplayerClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .create(mJoinedRoomConfig);
        }

        //INVITATIONS
        if (requestCode == RC_INVITATION_INBOX) {
            if (resultCode != Activity.RESULT_OK) {
                // Canceled or some error.
                Log.d("ERROR", "Incorrect result code: " + resultCode);
                return;
            }
            if (data.getExtras() != null){
                invitation = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);
            }
            if (data.getExtras() != null){
            Log.d("THIS", "data: " + data.getExtras().toString());
            }
            if (invitation != null) {

                Log.d("MESSAGE", "" + mMessageReceivedHandler);
                Log.d("THIS", "thisActivity: " + thisActivity);
                Log.d("THIS", "this: " + this);


                RoomConfig.Builder builder = RoomConfig.builder(mRoomUpdateCallback)
                        .setInvitationIdToAccept(invitation.getInvitationId())
                        .setOnMessageReceivedListener(mMessageReceivedHandler)
                        .setRoomStatusUpdateCallback(mRoomStatusCallbackHandler);
                mJoinedRoomConfig = builder
                        //.setOnMessageReceivedListener(mMessageReceivedHandler)
                        .build();
                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(this))
                        .join(mJoinedRoomConfig);
                // prevent screen from sleeping during handshake
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }


        //WAITING ROOM

        if (requestCode == RC_WAITING_ROOM) {

            if (resultCode == Activity.RESULT_OK) {
                // Start the game!
                Log.d("ROOM", "Starting game");

                Log.d("ROOM", "Your id: " + mMyParticipantId);
                playerIndexString = "";
                for (String player : mRoom.getParticipantIds()) {
                    playerIndexString += "," + player;
                }
                Log.d("ROOMSTATUS", "PlayerIndex: " + playerIndexString);

                firstTurn = true; //Have to be set here in case a player wishes to play two games in a row.
                mID = "pIds";
                msg = mID + "|" + playerIndexString;
                sendToAllReliably(msg);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("WARNING", "Someone clicked the back-button. Therefore everyone is forced out of the room and the game is closed");

                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(this))
                        .leave(mJoinedRoomConfig, mRoom.getRoomId());
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else if (resultCode == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
                // player wants to leave the room.
                Log.d("WARNING", "Someone left the room. Therefore everyone is forced out of the room and the game is closed");

                Games.getRealTimeMultiplayerClient(thisActivity,
                        GoogleSignIn.getLastSignedInAccount(this))
                        .leave(mJoinedRoomConfig, mRoom.getRoomId());
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }

        //DRAWING ACTIVITY

        if (requestCode == DRAWING_CODE || requestCode == PHRASE_CODE) {
            turn = false;
            Log.d("ROOM", "RECUSETCODE = " + DRAWING_CODE);
            Log.d("ROOM", "RESULTCODE = " + resultCode);
            if (resultCode != Activity.RESULT_OK) {

            }else {
                Log.d("ROOM", "Activity finished with code = ok");

                if(nextTurn()){
                    //Needs to wait to be sure that all players get their data
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            setParticipantReady(gameId, true);
                            mID = "ready";
                            msg = mID + "|" + gameId;
                            sendToAllReliably(msg);
                        }
                    }, 1000);   //1 second to let everyone catch up

                }else{
                    Log.d("ROOM", "GAME IS ENDING");
                }
            }
        }

        if(requestCode == PHRASE_CODE){
            Log.d("ROOM", "PHRASE ENDED");
        }

    }

    /**
     * Broadcasts a message to all users inn the room
     *
     * @param sMessage - the message to be converted to byte and broadcasted
     */
    void sendToAllReliably(String sMessage) {
        byte[] message = sMessage.getBytes();
        for (final String participantId : mRoom.getParticipantIds()) {
            if (!participantId.equals(mMyParticipantId)) {
                Task<Integer> task = Games.
                        getRealTimeMultiplayerClient(this, GoogleSignIn.getLastSignedInAccount(this))
                        .sendReliableMessage(message, mRoom.getRoomId(), participantId,
                                handleMessageSentCallback).addOnCompleteListener(new OnCompleteListener<Integer>() {
                            @Override
                            public void onComplete(@NonNull Task<Integer> task) {
                                Log.d("MESSAGE", "sendToAll: Message sent");
                            }
                        });
            }
        }
    }
    
    /**
     * Changes the turn and determines if the game ends or not
     * @return - true if there is a next turn. - false if the game ends
     */
    private boolean nextTurn(){
        Boolean returnValue = true;
        turnCounter ++;

        //Changes the turn
        gameId = (gameId++ >= (playerArray.length - 1)) ? 1 : gameId;
        Log.d("ROOM", "Game id : " + Integer.toString(gameId));
        //Ends the game it the conditions are met
        if (!firstTurn && playerArray[gameId].equals(mMyParticipantId)) {
            Log.d("ROOM", "GAME ENDED!");
            mID = "end";
            sendToAllReliably(mID + "|" + "Game ended");
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Switches to different activities based on an input
     * @param message - a String containing all of the player ids. Only used the first time this method is called.
     */
    private void activitySwitch(String message) {
        setPhrase("");

        if (firstTurn) {
            playerIndexString = "";
            playerArray = message.split(",");
            readyList = new boolean[playerArray.length -1];
            int counter = 0;
            for (String player : playerArray) {
                Log.d("ROOM", "hei! i got these players: " + player);
                if (player.equals(mMyParticipantId)) {
                    gameId = counter;
                    Log.d("ROOM", "I have game nr: " + Integer.toString(counter));
                    if(gameId != playerArray.length-1){
                        nextPlayerId = playerArray[gameId + 1];
                    }else{
                        nextPlayerId = playerArray[1];
                    }
                }
                counter++;
                firstTurn = false;
            }
            setPhrase("first turn");
        }
        if (turnCounter%2 == 1) {

            Log.d("TURN", "TURNCOUNTER: " + Integer.toString(turnCounter));
            databaseReference.child(mRoom.getRoomId()).child(Integer.toString(gameId)).child(nextPlayerId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d("ROOM", "FETCHING: GAMEID: " + gameId + " PARTICIPANTID: " + playerArray[gameId]);
                    if(turnCounter%2 == 1) { // Fetching phrase every odd-numbered turn. Same firebase listener in phrase_activity to get the image string
                        if(dataSnapshot.getValue() != null) {
                            setPhrase(String.valueOf(dataSnapshot.getValue()));
                        }
                        if (phrase != null){
                            Log.d("ROOM", "PHRASE NOT NULL: " + phrase);
                            Intent i = new Intent(GameTest.this, Paint.class);
                            i.putExtra("phrase", phrase);
                            i.putExtra("firstTurn", firstTurn);
                            i.putExtra("participantId", mMyParticipantId);
                            i.putExtra("roomId", mRoom.getRoomId());
                            i.putExtra("gameId", Integer.toString(gameId));
                            Log.d("ROOM:  ", "DRAWINGGGGGGG");
                            turn = true;
                            startActivityForResult(i, DRAWING_CODE);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("FIREBASE", "Read of firebase failed");
                }
            });
        } else {
            Log.d("ROOM:  ", "GUESSSINGG");
            turn = true;
            Intent drawingIntent = new Intent(GameTest.this, Phrase_Activity.class);
            drawingIntent.putExtra("drawing", image);
            drawingIntent.putExtra("participantId", mMyParticipantId);
            drawingIntent.putExtra("roomId", mRoom.getRoomId());
            drawingIntent.putExtra("gameId", Integer.toString(gameId));
            drawingIntent.putExtra("fetchId", nextPlayerId);
            startActivityForResult(drawingIntent, PHRASE_CODE);
        }
    }

    /**
     * sets a players ready status
     * @param id - the id of the player who's status you want to change
     * @param status - the status you want to set
     */
    private void setParticipantReady(int id, boolean status){
        int tempId = id - 1;
        Log.d("ROOM", "Player: " + tempId + " is ready (you)");
        this.readyList[tempId] = status;
    }

    /**
     * sets a players ready status - used when you receive data from another player
     * @param status - the status you want to set
     * @param message - the message containing the users id
     */
    private void setParticipantReady (String message, boolean status){
        int id = Integer.parseInt(message) - 1;
        Log.d("ROOM", "Player: " + id + " is ready (not you)");
        this.readyList[id] = status;
    }

    /**
     * checks if all the players are ready
     * @return - true if all players are ready. - false if not all players are ready
     */
    private boolean arePlayersReady(){
        int readyCounter = 0;
        for(Boolean status: readyList){
            if(status){
                readyCounter++;
            }
        }

        if(readyCounter == readyList.length){
        Log.d("ROOM", "All players are ready");
            for(Boolean status: readyList){
                status = false;
            }
            return true;
        }else{
            Log.d("ROOM", "Not all players are ready jet");
            return false;
        }
    }

    /**
     * sets the phrase
     * @param phrase - the string to set phrase with
     */
    private void setPhrase(String phrase){
        this.phrase = phrase;
    }

    /**
     * Sets the image to an encoded string
     * @param image - the encoded string to set image with
     */
    private void setImage(String image){
        this.image = image;
    }
}
