package no.ntnu.imt3673.group9;

/**
 * Created by marts on 30.04.2018.
 * Holds data retrieved from the leaderboard
 */

public class LeaderboardData {
    private int rank;
    private int score;
    private String name;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
