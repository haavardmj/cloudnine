package no.ntnu.imt3673.group9;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesCallbackStatusCodes;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.OnRealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateCallback;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: WIP class to contain quickgame.
 * @See https://github.com/playgameservices/android-basic-samples/blob/master/ButtonClicker/src/main/java/com/google/example/games/bc/MainActivity.java
 */
public class Quickgame {
    private final static String TAG = "Quickgame";
    private final static int MIN_PLAYERS = 2;
    private final static int MAX_PLAYERS = 2;

    // Things from Mainscreen
    private Activity mActivityReference;
    private Context mContext;

    private RoomUpdateCallback mRoomUpdateCallback;
    private OnRealTimeMessageReceivedListener mOnRealTimeMessageReceivedListener;
    private RoomStatusUpdateCallback mRoomStatusUpdateCallback;
    private GoogleSignInAccount mGoogleSignInAccount;
    private RoomConfig mRoomConfig;

    private String mRoomId = null;  //null = not playing
    private String mMyId = null;    //My participant ID in the currently active game
    ArrayList<Participant> mParticipants = null;

    public Quickgame(Activity myActivityReference) {
        this.mActivityReference = myActivityReference;
        this.mGoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(myActivityReference);
    }

    public void startQuickGame() {
        createRoomUpdateCallback();
        createMessageReceivedHandler();
        createRoomStatusUpdateCallback();
        // auto-match criteria to invite one random automatch opponent.
        // You can also specify more opponents (up to 3).
        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_PLAYERS, MAX_PLAYERS, 0);

        // build the room config:
        RoomConfig roomConfig =
                RoomConfig.builder(mRoomUpdateCallback)
                        .setOnMessageReceivedListener(mOnRealTimeMessageReceivedListener)
                        .setRoomStatusUpdateCallback(mRoomStatusUpdateCallback)
                        .setAutoMatchCriteria(autoMatchCriteria)
                        .build();

        // prevent screen from sleeping during handshake
        mActivityReference.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Save the roomConfig so we can use it if we call leave().
        mRoomConfig = roomConfig;

        // create room:
        Games.getRealTimeMultiplayerClient(mActivityReference, mGoogleSignInAccount)
                .create(roomConfig);
    }

    /**
     * TODO: Give content
     */
    private void createRoomStatusUpdateCallback() {
        mRoomStatusUpdateCallback = new RoomStatusUpdateCallback() {

            @Override
            public void onConnectedToRoom(@Nullable Room room) {
                Log.d(TAG, "onConnectedToRoom.");

                //get participants and my ID:
                if (room != null) {
                    mParticipants = room.getParticipants();
                }

                if (mRoomId == null) {
                    mRoomId = room.getRoomId();
                }
            }

            @Override
            public void onRoomConnecting(@Nullable Room room) {
                updateRoom(room);
            }

            @Override
            public void onRoomAutoMatching(@Nullable Room room) {
                updateRoom(room);
            }

            @Override
            public void onPeerInvitedToRoom(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onPeerDeclined(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onPeerJoined(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onPeerLeft(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onDisconnectedFromRoom(@Nullable Room room) {

            }

            @Override
            public void onPeersConnected(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onPeersDisconnected(@Nullable Room room, @NonNull List<String> list) {
                updateRoom(room);
            }

            @Override
            public void onP2PConnected(@NonNull String s) {

            }

            @Override
            public void onP2PDisconnected(@NonNull String s) {

            }
        };
    }

    /**
     * TODO: Give functionality to this method. Just prints values upon triggers so far.
     */
    private void createMessageReceivedHandler() {
        mOnRealTimeMessageReceivedListener = new OnRealTimeMessageReceivedListener() {
            @Override
            public void onRealTimeMessageReceived(@NonNull RealTimeMessage realTimeMessage) {
                Log.d(TAG, "onRealTimeMessageReceived RealtimeMessage: "+realTimeMessage);
            }
        };
    }

    /**
     * TODO: Give functionality to this method. Just prints values upon triggers so far.
     */
    private void createRoomUpdateCallback() {
        mRoomUpdateCallback = new RoomUpdateCallback() {

            @Override
            public void onRoomCreated(int i, @Nullable Room room) {
                Log.d(TAG, "onRoomCreated(" + i + ", " + room + ")");
                if (i != GamesCallbackStatusCodes.OK) {
                    Log.e(TAG, "*** Error: onRoomCreated, status " + i);
                    return;
                }

                mRoomId = room.getRoomId();
                //showWaitingRoom(room);
            }

            @Override
            public void onJoinedRoom(int i, @Nullable Room room) {
                Log.d(TAG, "onJoinedRoom(" + i + ", " + room + ")");
                if (i != GamesCallbackStatusCodes.OK) {
                    Log.e(TAG, "*** Error: onRoomConnected, status " + i);
                    return;
                }

                //showWaitingRoom(room); TODO: Make waiting room while waiting for game to start
            }

            @Override
            public void onLeftRoom(int i, @NonNull String s) {
                Log.d(TAG, "onLeftRoom, code " + i);
               goToMainscreen();
            }

            @Override
            public void onRoomConnected(int i, @Nullable Room room) {
                Log.d(TAG, "onRoomConnected(" + i + ", " + room + ")");
                if (i != GamesCallbackStatusCodes.OK) {
                    Log.e(TAG, "*** Error: onRoomConnected, status " + i);
                    return;
                }
                updateRoom(room);
            }
        };
    }

    private void goToMainscreen() {
        Log.d(TAG, "Going back to mainscreen, implement later");
        //TODO: Gå tilbake til mainscreen
    }

    private void updateRoom(Room room) {
        if (room != null) {
            mParticipants = room.getParticipants();
        }
        if (mParticipants != null) {
            Log.d(TAG, "Update players in waiting room, implement later");
            //TODO: Oppdater antall spillere i venterom (?)
        }
    }
}
