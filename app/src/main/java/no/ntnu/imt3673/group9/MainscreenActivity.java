package no.ntnu.imt3673.group9;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class MainscreenActivity extends AppCompatActivity {

    //Google forResult codes
    private static final int RC_ACHIEVEMENT_UI = 9005; // Var 9003
    private static final int RC_LEADERBOARD_UI = 9004;

    //Achievement client
    private AchievementsClient achievementsClient;
    //Highscore data
    private LeaderboardData lbd;
    //
    GoogleSignInAccount mGoogleSignInAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen);

        //Get signed in user
        mGoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);

        achievementsClient =  Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this));

        Button achievementsBtn = findViewById(R.id.achivements);
        achievementsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAchievements();
            }
        });

        Button leaderboardBtn = findViewById(R.id.leaderboard);
        leaderboardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLeaderboard();
            }
        });

        Button joinGame = findViewById(R.id.joinGame);
        joinGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameTest();
            }
        });

        Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });
    }

    /**
     * Google signout. Sends the user back to the Login activity
     */
    private void signOut() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(this,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        signInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // at this point, the user is signed out.
                        Intent i = new Intent(MainscreenActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                });
    }


    private void gameTest(){
        Intent intent = new Intent(this, GameTest.class);
        startActivity(intent);
    }

    /**
     * Displays achievements on Google Play Games
     */
    void showAchievements(){
        achievementsClient.getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_ACHIEVEMENT_UI);
                    }
                });
    }

    /**
     * Displays leaderboard on Google Play Games
     */
    private void showLeaderboard() {
        //checkIfTop10();
        Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .getLeaderboardIntent(getString(R.string.leaderboard_leaderboards))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_LEADERBOARD_UI);
                    }
                });
    }

    /**
     * Gets the top 10 scores and checks if the player is among them
     * @return -
     */
   private LeaderboardData checkIfTop10(){
        lbd = new LeaderboardData();

        Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .loadCurrentPlayerLeaderboardScore("CgkI6t28w-AOEAIQAQ", 2, 0).addOnSuccessListener(new OnSuccessListener<AnnotatedData<LeaderboardScore>>() {
            @Override
            public void onSuccess(AnnotatedData<LeaderboardScore> leaderboardScoreAnnotatedData) {

                lbd.setName(leaderboardScoreAnnotatedData.get().getScoreHolderDisplayName());
                lbd.setRank((int) leaderboardScoreAnnotatedData.get().getRank());
                lbd.setScore(Integer.parseInt(leaderboardScoreAnnotatedData.get().getDisplayScore()));
                Log.d("SUCCSESS", "lbd name2: "+lbd.getName());

            }
            //Log.d("SUCCSESS", "lbd name3: "+lbd.getName());
        });
        Log.d("SUCCSESS", "lbd name: "+lbd.getName());
       return lbd;
   }

}
