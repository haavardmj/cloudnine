# README #

## Authors ##
* Thomas B�rli - 470117
* Vetle Lundsvoll - 471220
* Martin trehj�rningen Harestad - 470217
* Martin Stenen - 471191
* H�vard Myrvold Johannessen - 769809 

## The Project ##

The project is to create a drawing/phrase app. 
Multiple players shuld be able to play a game together, at the same time, where the objective is to draw a phrase.
Send the drawing to the next player. The next player then have to guess what the drawing is supposed to be.
The guessed phrase is then sent to the next player, who then has to draw the recieved phrase.
The game continues like this til the player recieves a drawing/phrase of their original drawing.
  
Since all of the multiplayer rooms are handeld by Google Play Games Services, the players can play together frome anywhere as long as they have an internet conection.

## Set up ##

### In Android Studio ###

* Right click the app folder in the project overview 
* Select "Open Module Settings"
* Navigate to the Signing tab
* Change the path for the "Store File" to go to the keystore.jks in your local project
* Sync the gradle (Should be done automaticly)

You also need to switch from debug-mode to release-mode. You can do this by:
* Rightclick the square in the bottom left corner.
* Click on Build Variants
* Change the buld variant to release
* Let the gradle build (Should be done automaticly)

### In the Console ###

In order to login to the app you need to have an Google user (gmail), and for that user to be added as a test user in our console.
We have already added these users to our console:

* nowostawski@gmail.com 
* gjovikcampus@gmail.com

If you need more users added, contact one of the group members.

### Quick Fix For Some Issues ###

Sometimes it is necessary to close and rebuild the app in order to get some of the multiplayer aspects to work.
This is becasue some of the variables/data is not always reset prpperly after a game has ended, which might crash the app.

### Documented Warnings ###

* Google Play Games signInClient gives us a LOT of linter warnings. Since these where added by the code provided in the Developer Examples have the group let the wrnings stay as tamping with the code migth brake the application (and we do not have the time to go through the Play Game error-searching process again).

* The Developer Examples code also gives some may produce java.lang.Nulpoinetexeption. These have been left in for the same reasons as above.

* String concatenation  in a loop to constuct an array of players

* Boolean status is always true, this code is from the sample code and the group were more focused on getting a working prototype.

* onTuchEvent warning - we want to register clicks as attempts at drawing. Therefore we use the onTouchEvent instead of the performClick event.
